<?php

namespace AtsBundle\Entity;



use Doctrine\ORM\Mapping as ORM;

/**
 * Offre
 *
 * @ORM\Table(name="offre")
 * @ORM\Entity(repositoryClass="AtsBundle\Repository\OffreRepository")
 */
class Offre
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="intituleOffre", type="string", length=255)
     */
    private $intituleOffre;

    /**
     * @var string
     *
     * @ORM\Column(name="statusOffre", type="string", length=255)
     */
    private $statusOffre;

    /**
     * @var int
     *
     * @ORM\Column(name="prix", type="bigint")
     */
    private $prix;

    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string", length=255)
     */
    private $photo;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set intituleOffre
     *
     * @param string $intituleOffre
     *
     * @return Offre
     */
    public function setIntituleOffre($intituleOffre)
    {
        $this->intituleOffre = $intituleOffre;

        return $this;
    }

    /**
     * Get intituleOffre
     *
     * @return string
     */
    public function getIntituleOffre()
    {
        return $this->intituleOffre;
    }

    /**
     * Set statusOffre
     *
     * @param string $statusOffre
     *
     * @return Offre
     */
    public function setStatusOffre($statusOffre)
    {
        $this->statusOffre = $statusOffre;

        return $this;
    }

    /**
     * Get statusOffre
     *
     * @return string
     */
    public function getStatusOffre()
    {
        return $this->statusOffre;
    }

    /**
     * Set prix
     *
     * @param integer $prix
     *
     * @return Offre
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return int
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set photo
     *
     * @param string $photo
     *
     * @return Offre
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Offre
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * @ORM\ManyToOne(targetEntity="AtsBundle\Entity\Demande", inversedBy="offres")
     * @ORM\JoinColumn(nullable=true)
     */
    private $demande;

    /**
     * @return mixed
     */
    public function getDemande()
    {
        return $this->demande;
    }

    /**
     * @param mixed $demande
     */
    public function setDemande($demande)
    {
        $this->demande = $demande;
    }


    /**
     * @ORM\ManyToOne(targetEntity="AtsBundle\Entity\Membre", inversedBy="offres")
     * @ORM\JoinColumn(nullable=true)
     */
    private $membre;

    /**
     * @return mixed
     */
    public function getMembre()
    {
        return $this->membre;
    }

    /**
     * @param mixed $membre
     */
    public function setMembre($membre)
    {
        $this->membre = $membre;
    }



}

