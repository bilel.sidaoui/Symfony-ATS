<?php

namespace AtsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Membre
 *
 * @ORM\Table(name="membre")
 * @ORM\Entity(repositoryClass="AtsBundle\Repository\MembreRepository")
 */
class Membre
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="adr_postale", type="string", length=255,nullable=true)
     */
    private $adrPostale;

    /**
     * @var string
     *
     * @ORM\Column(name="tel", type="string", length=255,nullable=true)
     */
    private $tel;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=255,nullable=true)
     */
    private $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="msg_presentation", type="string", length=255,nullable=true)
     */
    private $msgPresentation;

    /**
     * @var boolean
     *
     * @ORM\Column(name="Confirmed", type="boolean", length=1,nullable=true)
     */
    private $confirmed;

    /**
     * @ORM\OneToMany(targetEntity="AtsBundle\Entity\Offre", mappedBy="membre")
     */
    private $offres;

    /**
     * @ORM\OneToMany(targetEntity="AtsBundle\Entity\Message", mappedBy="destinataire")
     */
    private $messages_received;

    /**
     * @ORM\OneToMany(targetEntity="AtsBundle\Entity\Message", mappedBy="destinateur")
     */
    private $messages_sent;

    public function __construct()
    {
        $this->offres = new ArrayCollection();
    }



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Membre
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Membre
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Membre
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set adrPostale
     *
     * @param string $adrPostale
     *
     * @return Membre
     */
    public function setAdrPostale($adrPostale)
    {
        $this->adrPostale = $adrPostale;

        return $this;
    }

    /**
     * Get adrPostale
     *
     * @return string
     */
    public function getAdrPostale()
    {
        return $this->adrPostale;
    }

    /**
     * Set tel
     *
     * @param string $tel
     *
     * @return Membre
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Get tel
     *
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return Membre
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return Membre
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set msgPresentation
     *
     * @param string $msgPresentation
     *
     * @return Membre
     */
    public function setMsgPresentation($msgPresentation)
    {
        $this->msgPresentation = $msgPresentation;

        return $this;
    }

    /**
     * Get msgPresentation
     *
     * @return string
     */
    public function getMsgPresentation()
    {
        return $this->msgPresentation;
    }

    /**
     * Set confirmed
     *
     * @param boolean $confirmed
     *
     * @return Membre
     */
    public function setConfirmed($confirmed)
    {
        $this->confirmed = $confirmed;

        return $this;
    }

    /**
     * Get confirmed
     *
     * @return boolean
     */
    public function getConfirmed()
    {
        return $this->confirmed;
    }

    /**
     * Add offre
     *
     * @param \AtsBundle\Entity\Offre $offre
     *
     * @return Membre
     */
    public function addOffre(\AtsBundle\Entity\Offre $offre)
    {
        $this->offres[] = $offre;

        return $this;
    }

    /**
     * Remove offre
     *
     * @param \AtsBundle\Entity\Offre $offre
     */
    public function removeOffre(\AtsBundle\Entity\Offre $offre)
    {
        $this->offres->removeElement($offre);
    }

    /**
     * Get offres
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOffres()
    {
        return $this->offres;
    }

    /**
     * Add messagesReceived
     *
     * @param \AtsBundle\Entity\Message $messagesReceived
     *
     * @return Membre
     */
    public function addMessagesReceived(\AtsBundle\Entity\Message $messagesReceived)
    {
        $this->messages_received[] = $messagesReceived;

        return $this;
    }

    /**
     * Remove messagesReceived
     *
     * @param \AtsBundle\Entity\Message $messagesReceived
     */
    public function removeMessagesReceived(\AtsBundle\Entity\Message $messagesReceived)
    {
        $this->messages_received->removeElement($messagesReceived);
    }

    /**
     * Get messagesReceived
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessagesReceived()
    {
        return $this->messages_received;
    }

    /**
     * Add messagesSent
     *
     * @param \AtsBundle\Entity\Message $messagesSent
     *
     * @return Membre
     */
    public function addMessagesSent(\AtsBundle\Entity\Message $messagesSent)
    {
        $this->messages_sent[] = $messagesSent;

        return $this;
    }

    /**
     * Remove messagesSent
     *
     * @param \AtsBundle\Entity\Message $messagesSent
     */
    public function removeMessagesSent(\AtsBundle\Entity\Message $messagesSent)
    {
        $this->messages_sent->removeElement($messagesSent);
    }

    /**
     * Get messagesSent
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessagesSent()
    {
        return $this->messages_sent;
    }





    /**
     * @ORM\OneToMany(targetEntity="AtsBundle\Entity\Demande", mappedBy="membre")
     */
    private $demandes;
}
