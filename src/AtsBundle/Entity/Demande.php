<?php

namespace AtsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Demande
 *
 * @ORM\Table(name="demande")
 * @ORM\Entity(repositoryClass="AtsBundle\Repository\DemandeRepository")
 */
class Demande
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @var string
     *
     * @ORM\Column(name="intituleDemande", type="string", length=255)
     */
    private $intituleDemande;


    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="lieu", type="string", length=255)
     */
    private $lieu;

    /**
     * @var string
     *
     * @ORM\Column(name="budget", type="string", length=255)
     */
    private $budget;


    /**
     * @var string
     *
     * @ORM\Column(name="date", type="string", length=255)
     */
    private $date;

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getLieu()
    {
        return $this->lieu;
    }

    /**
     * @param string $lieu
     */
    public function setLieu($lieu)
    {
        $this->lieu = $lieu;
    }

    /**
     * @return string
     */
    public function getBudget()
    {
        return $this->budget;
    }

    /**
     * @param string $budget
     */
    public function setBudget($budget)
    {
        $this->budget = $budget;
    }

    /**
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }









    /**
     * @ORM\OneToMany(targetEntity="AtsBundle\Entity\Offre", mappedBy="demande")
     */
    private $offres;
    public function __construct()
    {
        $this->offres = new ArrayCollection();
    }

    /**
     * @return Collection|Offre[]
     */
    public function getOffres()
    {
        return $this->offres;
    }

    /**
     * @return string
     */
    public function getIntituleDemande()
    {
        return $this->intituleDemande;
    }

    /**
     * @param string $intituleDemande
     */
    public function setIntituleDemande($intituleDemande)
    {
        $this->intituleDemande = $intituleDemande;
    }

    /**
     * @ORM\ManyToOne(targetEntity="AtsBundle\Entity\Membre", inversedBy="demandes")
     * @ORM\JoinColumn(nullable=true)
     */
    private $membre;




}

