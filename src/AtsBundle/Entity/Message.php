<?php

namespace AtsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Message
 *
 * @ORM\Table(name="message")
 * @ORM\Entity(repositoryClass="AtsBundle\Repository\MessageRepository")
 */
class Message
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu", type="text")
     */
    private $contenu;

    /**
     * @var bool
     *
     * @ORM\Column(name="lu", type="boolean")
     */
    private $lu;

    /**
     *
     * @ORM\ManyToOne(targetEntity="AtsBundle\Entity\Membre", inversedBy="messages_sent")
     * @ORM\JoinColumn(nullable=true)
     */
    private $destinateur;

    /**
     *
     * @ORM\ManyToOne(targetEntity="AtsBundle\Entity\Membre", inversedBy="messages_received")
     * @ORM\JoinColumn(nullable=true)
     */
    private $destinataire;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     *
     * @return Message
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set lu
     *
     * @param boolean $lu
     *
     * @return Message
     */
    public function setLu($lu)
    {
        $this->lu = $lu;

        return $this;
    }

    /**
     * Get lu
     *
     * @return bool
     */
    public function getLu()
    {
        return $this->lu;
    }

    /**
     * Set destinateur
     *
     * @param \stdClass $destinateur
     *
     * @return Message
     */
    public function setDestinateur($destinateur)
    {
        $this->destinateur = $destinateur;

        return $this;
    }

    /**
     * Get destinateur
     *
     * @return \stdClass
     */
    public function getDestinateur()
    {
        return $this->destinateur;
    }

    /**
     * Set destinataire
     *
     * @param \stdClass $destinataire
     *
     * @return Message
     */
    public function setDestinataire($destinataire)
    {
        $this->destinataire = $destinataire;

        return $this;
    }

    /**
     * Get destinataire
     *
     * @return \stdClass
     */
    public function getDestinataire()
    {
        return $this->destinataire;
    }
}
