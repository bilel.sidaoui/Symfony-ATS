<?php


namespace AtsBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
use AtsBundle\Entity\Offre;
use AtsBundle\Entity\Demande;
use Doctrine\Common\Collections\ArrayCollection;
class OffreController extends Controller

{
    /**
     * @Rest\Get("/offres")
     */
    public function getOffresAction(Request $request)
    {
        $offres = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AtsBundle:Offre')
            ->findAll();
        /* @var $offre Offre[] */

        return $offres;
    }


    /**
     * @Rest\Get("/offres/{id}")
     */
    public function getOffreAction(Request $request,$id)
    {
        $offres = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AtsBundle:Offre')
            ->find($request->get('id'));
        /* @var $offre Offre[] */

        return $offres;
    }



    /**
     * @Rest\Post("/Add/{idD}/{idM}")
     */
    public function addAction(Request $request, $idD,$idM)
    {

        $demande = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AtsBundle:Demande')
            ->find($request->get('idD'));

        $membre = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AtsBundle:Membre')
            ->find($request->get('idM'));

        $offre = new Offre();



        $offre->setIntituleOffre($request->get('intitule_offre'));
        $offre->setStatusOffre($request->get('status_offre'));
        $offre->setPrix($request->get('prix'));
        $offre->setPhoto($request->get('photo'));
        $offre->setDescription($request->get('description'));

        $offre->setDemande($demande);
        $offre->setMembre($membre);

        $em = $this->getDoctrine()->getManager();
        $em->persist($demande);
        $em->persist($membre);
        $em->persist($offre);
        $em->flush();

        return $offre;
    }


    /**
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
     * @Rest\Delete("/delete/{id}")
     */
    public function removeOffreAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $offre = $em->getRepository('AtsBundle:Offre')
            ->find($request->get('id'));
        /* @var $offre Offre*/

        $em->remove($offre);
        $em->flush();
    }




    /**
     * @Rest\View()
     * @Rest\Put("/update/{id}")
     */
    public function updateOffreAction( $id,Request $request)
    {
        $offre = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AtsBundle:Offre')
            ->find($request->get('id'));
        $offre->setIntituleOffre($request->get('intitule_offre'));
        $offre->setStatusOffre($request->get('status_offre'));
        $offre->setPrix($request->get('prix'));
        $offre->setPhoto($request->get('photo'));
        $offre->setDescription($request->get('description'));
        $sn = $this->getDoctrine()->getManager();
        $sn->merge($offre);
        $sn->flush();
        if (empty($offre)) {
            return new View("offre not found", Response::HTTP_NOT_FOUND);
        }
        return $offre;
    }


}