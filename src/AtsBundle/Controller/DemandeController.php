<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 01/01/2018
 * Time: 10:55
 */

namespace AtsBundle\Controller;


use AtsBundle\Entity\Demande;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;


use Symfony\Component\HttpFoundation\Request;

class DemandeController extends Controller
{
    /**
     * @Rest\Get("/demandes")
     */
    public function getDemandesAction(Request $request)
    {
        $demandes = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AtsBundle:Demande')
            ->findAll();
        /* @var $demande Demande[] */

        return $demandes;
    }


    /**
     * @Rest\Get("/demandes/{id}")
     */
    public function getDemandeeAction(Request $request,$id)
    {
        $demandes = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AtsBundle:Demande')
            ->find($request->get('id'));
        /* @var $demande Demande[] */

        return $demandes;
    }



    /**
     * @Rest\Post("/Add/{idO}/{idM}")
     */
    public function addAction(Request $request, $idO,$idM)
    {

        $offre = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AtsBundle:Offre')
            ->find($request->get('idO'));

        $membre = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AtsBundle:Membre')
            ->find($request->get('idM'));

        $demande = new Demande();



        $demande->setIntituleDemande($request->get('intituleDemande'));
        $demande->setStatus($request->get('status'));
        $demande->setLieu($request->get('lieu'));
        $demande->setDate($request->get('date'));
        $demande->setBudget($request->get('budget'));

        $offre->setOffre($offre);
        $membre->setMembre($membre);

        $em = $this->getDoctrine()->getManager();
        $em->persist($offre);
        $em->persist($membre);
        $em->persist($demande);
        $em->flush();

        return $demande;
    }


    /**
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
     * @Rest\Delete("/delete/{id}")
     */
    public function removeOffreAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $offre = $em->getRepository('AtsBundle:Demande')
            ->find($request->get('id'));
        /* @var $demande Demande*/

        $em->remove($demande);
        $em->flush();
    }




    /**
     * @Rest\View()
     * @Rest\Put("/update/{id}")
     */
    public function updateDemandeAction( $id,Request $request)
    {
        $demande = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AtsBundle:Demande')
            ->find($request->get('id'));
        $demande->setIntituleDemande($request->get('intituleDemande'));
        $demande->setStatus($request->get('status'));
        $demande->setLieu($request->get('lieu'));
        $demande->setDate($request->get('date'));
        $demande->setBudget($request->get('budget'));
        $sn = $this->getDoctrine()->getManager();
        $sn->merge($demande);
        $sn->flush();
        if (empty($demande)) {
            return new View("Demande not found", Response::HTTP_NOT_FOUND);
        }
        return $demande;
    }


}