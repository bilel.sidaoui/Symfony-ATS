<?php

namespace AtsBundle\Controller;

use AtsBundle\Entity\Membre;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;


use Symfony\Component\HttpFoundation\Request;

/**
 * Membre controller.
 *
 */
class MembreController extends Controller
{
    /**
     *
     * @Rest\Post("connexion")
     * @Rest\View()
     */
    public function connexion(Request $request)
    {
        $membre = $this->get('doctrine.orm.entity_manager')
            ->getRepository(Membre::class)
            ->findOneBy(
                array('email' => $request->get('email'),
                    'password' => hash('sha512', $request->get('password')) )
            );
        /* @var $membre Membre[] */

        if (empty($membre)) {
            return new View(-1, Response::HTTP_NOT_FOUND);
        }
        return $membre->getId();
    }

    /**
     * Lists all membre entities.
     *
     * @Rest\Get("/membres")
     */
    public function indexAction(Request $request)
    {


        $membres = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AtsBundle:Membre')
            ->findAll();
        /* @var $membre Membre[] */

        return $membres;
    }

    /**
     * Creates a new membre entity.
     *
     * @Rest\Post("/membres")
     */
    public function newAction(Request $request)
    {
        $membre = new Membre();
        $membre->setPrenom($request->get('prenom'));
        $membre->setNom($request->get('nom'));
        $membre->setEmail($request->get('email'));
        $membre->setAdrPostale($request->get('adr_postale'));
        $membre->setTel($request->get('tel'));
        $membre->setPassword(        hash('sha512', $request->get('password'))  );
        $membre->setVille($request->get('ville'));
        $membre->setMsgPresentation($request->get('msg_presentation'));
        $membre->setConfirmed($request->get('confirmed'));


        $em = $this->getDoctrine()->getManager();
        $em->persist($membre);
        $em->flush();

        return $membre;
    }

    /**
     * Finds and displays a membre entity.
     *
     * @Rest\Get("membres/{id}")
     * @Rest\View()
     */
    public function showAction(Request $request)
    {
        $membre = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AtsBundle:Membre')
            ->find($request->get('id'));
        /* @var $membre Membre[] */

        if (empty($membre)) {
            return new View("Membre not found", Response::HTTP_NOT_FOUND);
        }
        return $membre;
    }

    /**
     * Displays a form to edit an existing membre entity.
     * @Rest\Put("membres/{id}")

     * @Rest\View()
     */
    public function editAction(Request $request)
    {

        $membre = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AtsBundle:Membre')
            ->find($request->get('id'));

        if (empty($membre)) {
            return new View("Membre not found", Response::HTTP_NOT_FOUND);
        }

        $membre->setPrenom($request->get('prenom'));
        $membre->setNom($request->get('nom'));
        $membre->setEmail($request->get('email'));
        $membre->setAdrPostale($request->get('adr_postale'));
        $membre->setTel($request->get('tel'));
        $membre->setPassword($request->get('password'));
        $membre->setVille($request->get('ville'));
        $membre->setMsgPresentation($request->get('msg_presentation'));
        $membre->setConfirmed($request->get('confirmed'));

        $sn = $this->getDoctrine()->getManager();
        $sn->merge($membre);
        $sn->flush();
        if (empty($membre)) {
            return new View("Membre not found", Response::HTTP_NOT_FOUND);
        }
        return $membre;
    }

    /**
     * Deletes a membre entity.
     *
     * @Rest\Delete("membres/{id}")
     */
    public function deleteAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $membre = $em->getRepository('AtsBundle:Membre')
            ->find($request->get('id'));
        /* @var $membre Membre*/

        if (empty($membre)) {
            return new View("Membre not found", Response::HTTP_NOT_FOUND);
        }

        $em->remove($membre);
        $em->flush();
    }



}
