<?php

namespace AtsBundle\Controller;

use AtsBundle\Entity\Message;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;


/**
 * Message controller.
 *
 */
class MessageController extends Controller
{
    /**
     * Lists all message entities.
     *
     * @Rest\Get("/messages")
     */
    public function indexAction()
    {
        $messages = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AtsBundle:Message')
            ->findAll();
        /* @var $message Message[] */

        return $messages;
    }

    /**
     * Creates a new message entity.
     *
     * @Rest\Post("messages/{idDestinateur}/{idDestinataire}")
     */
    public function newAction(Request $request)
    {
        $destinateur = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AtsBundle:Membre')
            ->find($request->get('idDestinateur'));

        $destinataire = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AtsBundle:Membre')
            ->find($request->get('idDestinataire'));

        $messages = new Message();
        if (empty($destinateur)) {
            return new View("Destinateur not found", Response::HTTP_NOT_FOUND);
        }
        if (empty($destinataire)) {
            return new View("Destinataire not found", Response::HTTP_NOT_FOUND);
        }


        $messages->setContenu($request->get('contenu'));
        $messages->setLu($request->get('lu'));

        $messages->setDestinateur($destinateur);
        $messages->setDestinataire($destinataire);

        $em = $this->getDoctrine()->getManager();
        $em->persist($destinateur);
        $em->persist($destinataire);
        $em->persist($messages);
        $em->flush();

        return $messages;
    }

    /**
     * Finds and displays a message entity.
     *
     * @Rest\Get("/messages/{id}")
     */
    public function showAction(Request $request)
    {
        $message = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AtsBundle:Message')
            ->find($request->get('id'));
        /* @var $message Message[] */

        if (empty($message)) {
            return new View("message not found", Response::HTTP_NOT_FOUND);
        }

        return $message;
    }

    /**
     * Displays a form to edit an existing message entity.
     *
     * @Rest\View()
     * @Rest\Put("messages/{id}")
     */
    public function editAction(Request $request)
    {
        $message = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AtsBundle:Message')
            ->find($request->get('id'));

        if (empty($message)) {
            return new View("message not found", Response::HTTP_NOT_FOUND);
        }

        $message->setContenu($request->get('contenu'));
        $message->setLu($request->get('lu'));

        $sn = $this->getDoctrine()->getManager();
        $sn->merge($message);
        $sn->flush();

        return $message;
    }

    /**
     * Deletes a message entity.
     *
     * @Rest\Delete("messages/{id}")
     */
    public function deleteAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $message = $em->getRepository('AtsBundle:Message')
            ->find($request->get('id'));
        /* @var $message $message*/

        if (empty($message)) {
            return new View("message not found", Response::HTTP_NOT_FOUND);
        }

        $em->remove($message);
        $em->flush();
    }


}
